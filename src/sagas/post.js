import { call , put , takeLatest } from 'redux-saga/effects'

// Import Dispatch Type
import {
    FETCH_POST_REQUEST ,
    FETCH_POST_SUCCESS ,
    FETCH_POST_FAILURE ,
    NEW_POST_REQUEST ,
    NEW_POST_SUCCESS ,
    NEW_POST_FAILURE
} from '../actions/types'

// Import api
import { getPost , postPost } from '../apis/post'

export function* watchFetchPost () {
    yield takeLatest( FETCH_POST_REQUEST , fetchPost );
}

function* fetchPost(action){
    // Test API file
    //console.log("Worker fetchPost is starting");
    try{
        const data = yield call( getPost );
        yield put ({ type : FETCH_POST_SUCCESS , payload : data});
    } catch (error) {
        yield put ({ type : FETCH_POST_FAILURE })
    }

}

export function* watchNewPost () {
    yield takeLatest(NEW_POST_REQUEST , postNewPost);
}

function* postNewPost(action){
    console.log('worker is working : ' + JSON.stringify(action.payload));
    //const data = null;
    try {
        const data = yield call(postPost , action.payload);
        //console.log("call api successfully");
        yield put ({ type : NEW_POST_SUCCESS , payload : data });
    } catch (error) {
        console.log("Error : " + error.message);
        yield put ({ type : NEW_POST_FAILURE , message : error.message });
    }
        // async () => 
        // {
        // const res = await fetch("https://jsonplaceholder.typicode.com/posts" , {
        //         method : "POST" ,
        //         headers : {
        //             'content-type' : 'application/json'
        //         },
        //         body : JSON.stringify(action.payload)
        //     })
        // return res.json();
        // }
    

    //     return res;
    //         // .then(res => res.json())

    //         // .then(post => dispatch({
    //         //     type : NEW_POST,
    //         //     payload : post
    //         // }));
    //     }
    // )
    //yield put({ type : NEW_POST_SUCCESS , payload : data});

}