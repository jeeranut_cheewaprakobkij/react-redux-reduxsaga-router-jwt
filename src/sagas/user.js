import { put , call , takeLatest } from 'redux-saga/effects';

import jwt from 'jsonwebtoken';
// Import Dispatch Type
import { 
    LOGIN_REQUEST ,
    LOGIN_SUCCESS ,
    LOGIN_FAILURE,
    SET_CURRENT_USER
} from '../actions/types'
import {
    REGISTER_REQUEST ,
    REGISTER_SUCCESS , 
    REGISTER_FAILURE
} from '../actions/types'
import {
    LOGOUT_REQUEST ,
    LOGOUT_SUCCESS 

} from '../actions/types'

// Import api
import { postLogin , postRegister } from '../apis/user'

export function* watchLogin () {
    yield takeLatest(LOGIN_REQUEST , workerPostLogin );
}

function* workerPostLogin (action) {
    console.log("postLogin worker is running and data is :" + JSON.stringify(action.payload));
    try{
        const data = yield call (postLogin , action.payload);
        yield put ({ type : LOGIN_SUCCESS , payload : data});
        yield put ({ type : SET_CURRENT_USER , payload : jwt.decode(data.token)});
    } catch (error) {
        console.log(error.message);
        yield put ({ type : LOGIN_FAILURE , message : error.message });
    }
}

export function* watchRegister() {
    yield takeLatest(REGISTER_REQUEST , workerPostRegister );
}

function* workerPostRegister(action) {
    console.log("workerPostRegister is running and data is :" + JSON.stringify(action.payload))
    try {
        const data = yield call ( postRegister , action.payload)
        yield put ({ type : REGISTER_SUCCESS , payload : data})
    } catch (error) {
        console.log(error.message);
        yield put ({ type : REGISTER_FAILURE , message : error.message })
    }
}

export function* watchLogout(){
    yield takeLatest(LOGOUT_REQUEST , workerLogout);
}

function* workerLogout(){
    yield put ({ type : LOGOUT_SUCCESS });
}