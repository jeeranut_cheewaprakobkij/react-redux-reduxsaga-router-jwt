import { all } from 'redux-saga/effects';

// Import Watcher Function
import {
    watchFetchPost ,
    watchNewPost
} from './post'
import { 
watchRegister ,
watchLogin ,
watchLogout 
} from './user'

// Run Watcher
export function* rootSaga(){
    yield all([
        watchFetchPost(),
        watchNewPost(),
        watchLogin(),
        watchRegister(),
        watchLogout()
    ])
}

