import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Posts from '../components/Posts';
import PostForm from '../components/PostForm';
import Login from '../components/auth/Login';
import ProtectedRoute from '../components/auth/ProtectedRoute';
import Navbar from '../components/Navbar';
import Register from '../components/auth/Register';

import { setCurrentUser } from '../actions/user';
import { setAuthorizationToken } from '../apis/helper';
import jwt from 'jsonwebtoken';
import { connect } from 'react-redux';
    

    
class index extends React.Component {

componentDidMount(){
    if(localStorage.token){
        setAuthorizationToken(localStorage.token);
        this.props.setCurrentUser(jwt.decode(localStorage.token));
    }
}

render (){
    return (
        <div className="container">
            <Router >
            <Navbar />
                <Switch>
                    <Route exact path="/" render={ props => <Posts {...props} /> }  />
                    <Route exact path="/login" render={ props => <Login {...props} /> } />
                    <Route exact path="/postform" render={props => <PostForm {...props} /> } />
                    <Route exact path="/register" render={props => <Register {...props} /> } />
                    <ProtectedRoute
                        exact
                        path="/proute"
                        component={Posts}
                    />
                    <Route path="*" component={() => "404 Not Found !!!"} />
                </Switch>
            </Router>
        </div>
    )    
}

}

export default connect(null , {setCurrentUser})(index);