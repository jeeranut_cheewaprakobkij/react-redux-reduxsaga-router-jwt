import {  NEW_POST_SUCCESS , FETCH_POST_SUCCESS , FETCH_POST_REQUEST } from "../actions/types";

const initialState = {
    isLoading : false ,
    items : [] ,
    item : {}
}

export default function(state = initialState , action) {
    switch(action.type){
        case FETCH_POST_REQUEST :
            return {
                ...state,
                isLoading : true
            }
        case FETCH_POST_SUCCESS :
            //console.dir('reducer : ' + action.payload );
            return {
                ...state ,
                items : action.payload ,
                isLoading : false
            }
        case NEW_POST_SUCCESS :
            return {
                ...state ,
                item : action.payload ,
                isLoading : false
            }
        // case NEW_POST_FAILURE :
        //     return {
        //         ...state ,
        //         item : action.payload
        //     }
        default :
            return state;
    }

}