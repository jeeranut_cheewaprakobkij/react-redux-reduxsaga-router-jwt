import {
    LOGIN_REQUEST ,
    LOGIN_SUCCESS ,
    LOGIN_FAILURE ,
    REGISTER_REQUEST ,
    REGISTER_SUCCESS , 
    REGISTER_FAILURE ,
    LOGOUT_SUCCESS ,
    SET_CURRENT_USER
} from '../actions/types';
import isEmpty from 'lodash/isEmpty';

const initialState = {
    token : localStorage.getItem('token') ,
    isAuthenticated : false ,
    isLoading : false ,
    user : {}
};

export default function(state = initialState , action){
    switch(action.type){
        case LOGIN_REQUEST :
        case REGISTER_REQUEST :
            return {
                ...state ,
                isLoading : true
            };
        case SET_CURRENT_USER :
            console.log(action);
            return {
                ...state ,
                isAuthenticated : !isEmpty(action.payload) ,
                user : action.payload
            };
        case LOGIN_SUCCESS :
        case REGISTER_SUCCESS :
            localStorage.setItem('token' , action.payload.token);
            return {
                ...state , 
                ...action.payload ,
                isAuthenticated : true ,
                isLoading : false
            };
        case LOGIN_FAILURE :
        case REGISTER_FAILURE :
        case LOGOUT_SUCCESS :
            localStorage.removeItem('token');
            return {
                ...state ,
                token : null ,
                user : null ,
                isAuthenticated : false ,
                isLoading : false
            };
        default :
            return state;
    }
}