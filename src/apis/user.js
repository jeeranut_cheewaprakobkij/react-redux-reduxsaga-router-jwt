import axios from 'axios';
// import { setCurrentUser } from '../actions/user'
import { setAuthorizationToken } from './helper';

// import jwt from 'jsonwebtoken';

export const postLogin = async (payload) => {
    try {
        const { email , password } = payload;
        const response = await axios.post("http://localhost:5000/api/user/v1/login",{
            email , password
        });
        //console.log(response.data.token);
        setAuthorizationToken(response.data.token);
        // setCurrentUser(jwt.decode(response.data.token));
        return response.data;
    } catch (error) {
        console.log(error.message);
        throw error.message;
    }
}

export const postRegister = async (payload) => {
    try {
        const { email , name , password , password2 } = payload;
        const response = await axios.post(
            "http://localhost:5000/api/user/v1/register" ,
            { email , name , password , password2 }
        )
        return response.data;
    } catch (error) {
        console.log(error.message);
        throw error.message;
    }
}