import axios from 'axios';

// export const getPost = async () => {
//     try{
//         const response = await fetch("https://jsonplaceholder.typicode.com/posts");
//         const payload = await response.json();
//         return payload;
//     } catch (error){
//         console.log(error);
//         throw error;
//     }
// }

export const getPost = async () => {
    try {
        const response = await axios("https://jsonplaceholder.typicode.com/posts");
        return response.data;
    } catch (error) {
        console.log(error.message);
        throw error.message;
    }
}

// Test OK
// export const postPost = async (payload) => {
//     //console.log(JSON.stringify(action));
//     try{
//         const response = await fetch("https://jsonplaceholder.typicode.com/posts" , {
//                 method : "POST" ,
//                 headers : {
//                     'content-type' : 'application/json'
//                 },
//                 body : JSON.stringify(payload)
//             })
//         const data = await response.json();
//         //console.log(payload);
//         return data;
//     } catch (error) {
//         console.log(error);
//         throw error;
//     }
// }

export const postPost = async (payload) => {
        try {
        const { title , body } = payload;
        const response = await axios.post(
            'https://jsonplaceholder.typicode.com/posts' , 
            {title , body});
        return response.data;
    } catch (error){
        console.log(error);
        throw error;
    }
};