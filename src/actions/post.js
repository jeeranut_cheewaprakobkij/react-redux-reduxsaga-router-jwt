import { FETCH_POST_REQUEST , NEW_POST_REQUEST } from "./types";

// Dispatch Request fetch posts
export const fetchPostRequest = () => 
({
    type : FETCH_POST_REQUEST
})

export const newPostRequest = (payload) =>
({
    type : NEW_POST_REQUEST ,
    payload : payload
})

// export const fetchPosts = async () => {
//         //console.log('fetching');
//         const res = await fetch('https://jsonplaceholder.typicode.com/posts');
//         const data = await res.json();
//             //.then((res) => res.json());
//         return data;
//             // .then((posts) => dispatch({
//             //     type : FETCH_POSTS ,
//             //     payload : posts
//             // }));
    
// }

export const createPost = (postData) => ({
    type : NEW_POST_REQUEST ,
    payload : postData
    //console.log('action called');
    // fetch("https://jsonplaceholder.typicode.com/posts" , {
    //     method : "POST" ,
    //     headers : {
    //         'content-type' : 'application/json'
    //     },
    //     body : JSON.stringify(postData)
    // })
    // .then(res => res.json())
    // .then(post => dispatch({
    //     type : NEW_POST,
    //     payload : post
    // }));

})

