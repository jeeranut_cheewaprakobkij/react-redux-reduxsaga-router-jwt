import { LOGIN_REQUEST , REGISTER_REQUEST , LOGOUT_REQUEST, SET_CURRENT_USER } from './types';

export const setCurrentUser = (payload) => ({
    type : SET_CURRENT_USER ,
    payload : payload
})

export const loginRequest = (payload) => 
({
    type : LOGIN_REQUEST ,
    payload : payload
})

export const registerRequest = (payload) => ({
    type : REGISTER_REQUEST ,
    payload : payload
})

export const logoutRequest = () => ({
    type : LOGOUT_REQUEST
})