import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';

// Import Router
//import { BrowserRouter as Router , Switch , Route } from 'react-router-dom';

// Import components
// import Posts from './components/Posts';
// import PostForm from './components/PostForm';
// import Login from './components/auth/Login';
// import Navbar from './components/Navbar';

import Routes from './routes/index';
import store from './store';
// import { setAuthorizationToken } from './apis/helper';
// import { setCurrentUser } from './actions/user';
// import jwt from 'jsonwebtoken';


class App extends Component {


    render(){
      return (
        <Provider store={store}>
          <Routes />
        </Provider>
      )
    }
}

export default App;
