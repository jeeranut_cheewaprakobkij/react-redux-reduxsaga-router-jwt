import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink , Link } from 'react-router-dom'

import { logoutRequest } from '../actions/user'

class Navbar extends Component {

    onClick(e){
        e.preventDefault();
        this.props.logoutRequest();
    }

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
                <Link className="navbar-brand" to="/">Home</Link>
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink activeClassName="is-active" className="nav-link" to="/">Posts</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink activeClassName="is-active" className="nav-link" to="/postform">Post Form</NavLink>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/proute">Protected</Link>
                        </li>


                        { this.props.isAuthenticated ? 
                        <div></div> :
                        <li className="nav-item">
                            <Link className="nav-link" to="/register">Register</Link>
                        </li>
                        }
                        { this.props.isAuthenticated ? 
                            <li className="nav-item"><button onClick={this.onClick.bind(this)} className="btn btn-danger">Logout</button></li> :
                            <li className="nav-item">
                                <Link className="nav-link" to="/login">Login</Link>
                            </li>
                        }
                             
                    </ul>
                </nav>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated : state.user.isAuthenticated
})

export default connect(mapStateToProps , { logoutRequest })(Navbar);
