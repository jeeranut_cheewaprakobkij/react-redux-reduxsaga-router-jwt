import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchPostRequest } from '../actions/post';

class Posts extends Component {
    // No need anymore . We will move to use in redux to keep state and props
    // constructor(props){
    //     super(props);
    //     this.state = {
    //         posts : []
    //     }
    // }

    componentDidMount(){
        this.props.fetchPostRequest();
    }

    // This life cycle is : When it receives a new property(props) from state this will run
    componentWillReceiveProps(nextProps){
        if(nextProps.newPost){
            this.props.posts.unshift(nextProps.newPost);
        }
    }

    render() {
        const isLoading = this.props.isLoading;
        const postItems = this.props.posts.map(post => (
            <div key={post.id}>
                <h3>{post.title}</h3>
                <p>{post.body}</p>
            </div>
        ));

        return (
            <div>
                <h1>This is Post page</h1>
                {
                isLoading ? 
                <div>Data is loading ....</div> :
                postItems
                }
                
            </div>
        )
    }
}
Posts.propTypes = {
    //fetchPosts : PropTypes.func.isRequired,
    posts : PropTypes.array.isRequired,
    newPost : PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    isLoading : state.posts.isLoading ,
    posts : state.posts.items ,
    newPost : state.posts.item
});

// const mapDispatchToProps = (dispatch) => {
//     return {
//         onLoadComponent : () => dispatch({ type : "FETCH_POSTS" })
//     };
// };

export default connect( mapStateToProps , { fetchPostRequest } )(Posts);