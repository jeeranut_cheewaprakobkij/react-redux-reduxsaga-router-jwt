import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { newPostRequest } from '../actions/post'

class PostForm extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            title : '',
            body : ''
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    static propTypes = {
        newPostRequest : PropTypes.func.isRequired     
    };

    onChange(e) {
        this.setState({ [e.target.name] : e.target.value })
    }

    onSubmit(e) {
        e.preventDefault();

        const post = {
            title : this.state.title,
            body : this.state.body
        }

        this.props.newPostRequest(post);
        this.setState({ title : "" , body : ""});
    }

    render() {

        return (
            <div>
                <h1>Add Post</h1>
                <form onSubmit={this.onSubmit}>
                    <div>
                        <label >Title: </label>
                        <input type="text" name="title" onChange={this.onChange} value={this.state.title}></input>
                    </div>
                    <div>
                        <label >Body: </label>
                        <textarea name="body" onChange={this.onChange} value={this.state.body}></textarea>
                    </div>
                    <button type="submit">Submit</button>
                </form>
            </div>
        )
    }
}

// const mapStateToProps = (state) => ({
//     posts : state.posts
// })

// const mapDispatchToProps = (dispatch) => {

// };


export default connect( null  , { newPostRequest } )(PostForm);