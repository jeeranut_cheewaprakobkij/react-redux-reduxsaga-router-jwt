import React, { Component } from 'react'

export default class TestState extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count : 0 ,
            msg : "Default Val" ,
            input : ""
        }

        // this.increaseVal = this.increaseVal.bind(this);
        // this.onChange = this.onChange.bind(this);
        // this.resetVal = this.resetVal.bind(this);
    }

    onChange(e) {
        this.setState({[e.target.name] : e.target.value })
    }

    increaseVal(){
        this.setState({ count : this.state.count+this.state.input })
    }

    resetVal(){
        this.setState({ count : 0 , msg : "Reseted already"})
    }

    // increaseVal = () => {
    //     this.setState({ count : this.state.count + 1})
    // }

    render() {
        return (
            <div>
                <input name="input" type="text" onChange={this.onChange.bind(this)} value={this.state.input}></input>
                <button 
                className="btn btn-outline-primary" 
                type="button"
                onClick={this.increaseVal.bind(this)}
                >
                Count is : {this.state.count}
                </button>
                <button 
                className="btn btn-outline-primary" 
                type="button"
                onClick={this.resetVal.bind(this)}
                >
                {this.state.msg}
                </button>
            </div>
        )
    }
}
