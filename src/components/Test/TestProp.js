import React, { Component } from 'react'

export default class TestProp extends Component {
    render() {
        return (
            <div>
                <Entry label="Username" type="text" hint="username" />
                <Entry label="Password" type="password" hint="password"/>
            </div>
        )
    }
}

class Entry extends Component {
    constructor(props){
        super(props)
    }

    render() {
        return (
            <div>
                
                <label style={ { backgroundColor : "green"} } for={this.props.label}>{this.props.label} : </label>
                <input 
                type={this.props.type} 
                name={this.props.label} 
                placeholder={this.props.hint}
                ></input>
                
            </div>
        )
    }
}
