import React from 'react'
import { Route , Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

// export const ProtectedRoute = ({ component : Component , ...rest}) => {
//     return (
//         <Route {...rest} render={
//             (props) => {
//                 if (auth.isAuthenticated()){
//                     return <Component {...props} />;
//                 }
//                 else {
//                     return <Redirect to={{
//                         pathname : "/",
//                         state : {
//                             from : props.location
//                         }
//                     }} />
//                 }
//             }
//         } />
//     )
// }

// Test PrivateRoute component
class ProtectedRoute extends React.Component {

    constructor(props) {
        super(props);

        // this.state = {
        //     isLoading: true,
        //     isAuthenticated: false
        // };
        
        // Your axios call here


        // For success, update state like
        //this.setState(() => ({ isLoading: false, isLoggedIn: true }));

        // For fail, update state like
        //this.setState(() => ({ isLoading: false, isLoggedIn: false }));

    }

    componentDidMount(){
        // this.setState(() => ({ isLoading: false, isAuthenticated: true }));
    }


    render() {
        const isAuthenticated = this.props.isAuthenticated;
        const isLoading = this.props.isLoading;

        return isLoading ? null :
            isAuthenticated ?
            <Route path={this.props.path} component={this.props.component} exact={this.props.exact}/> :
            <Redirect to={{ pathname: '/login', state: { from: this.props.location } }} />

    }

}
const mapStateToProps = (state) => ({
    isAuthenticated : state.user.isAuthenticated ,
    isLoading : state.user.isLoading
})

export default connect(mapStateToProps , null)(ProtectedRoute);