import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

import { registerRequest } from '../../actions/user'

class Register extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            email : "",
            name : "",
            password : "",
            password2 : ""
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }
        

// OnChange function
onChange = (e) => {
    this.setState({ [e.target.name] : e.target.value });
}

// OnSubmit function
onSubmit = (e) => {
    e.preventDefault();

    const credentials = {
        email : this.state.email ,
        name : this.state.name ,
        password : this.state.password ,
        password2 : this.state.password2
    }

    this.props.registerRequest(credentials);

    this.setState({ email : "" , name : "" , password : "" , password2 : ""});
}

    render() {
        const isAuthenticated = this.props.isAuthenticated;
        if(isAuthenticated) {
            return (
                <Redirect to="/" />
            )
        }

        return (
            <div>
                <div className="container">
                <link rel="stylesheet" type="text/css" href="/style/styles.css" />
                    <div className="d-flex justify-content-center h-100">
                        <div className="card">
                        <div className="card-header">
                            <h3>Register</h3>
                            <div className="d-flex justify-content-end social_icon">
                            <span><i className="fab fa-facebook-square" /></span>
                            <span><i className="fab fa-google-plus-square" /></span>
                            <span><i className="fab fa-twitter-square" /></span>
                            </div>
                        </div>
                        <div className="card-body">
                            <form onSubmit={this.onSubmit}>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-user" /></span>
                                    </div>
                                    <input id="email" name="email" onChange={this.onChange} value={this.state.email} type="text" className="form-control" placeholder="Email@Email.com" />
                                </div>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-user" /></span>
                                    </div>
                                    <input id="name" name="name" onChange={this.onChange} value={this.state.name} type="text" className="form-control" placeholder="Your name" />
                                </div>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-key" /></span>
                                    </div>
                                    <input id="password" name="password" onChange={this.onChange} value={this.state.password} type="password" className="form-control" placeholder="Password" />
                                </div>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-key" /></span>
                                    </div>
                                    <input id="password2" name="password2" onChange={this.onChange} value={this.state.password2} type="password" className="form-control" placeholder="Confirm password" />
                                </div>
                                <div className="form-group">
                                    <input type="button" value="Login" className="btn float-right login_btn" />
                                </div>
                                <div className="form-group">
                                    <input type="submit" value="Register" className="btn float-right login_btn" />
                                </div>
                            </form>
                        </div>
                        <div className="card-footer">
                            <div className="d-flex justify-content-center links">
                            You have accounts already ? <a href="login">Login here</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated : state.user.isAuthenticated
})

export default connect(mapStateToProps , {registerRequest})(Register);