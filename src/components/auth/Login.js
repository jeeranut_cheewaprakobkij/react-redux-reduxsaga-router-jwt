import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { loginRequest } from '../../actions/user'

class Login extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             username : "",
             password : ""
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }
    
    onChange = (e) => {
        //console.log(e);
        this.setState({ [e.target.name] : e.target.value })
    }

    onSubmit = (e) => {
        e.preventDefault();

        const credentials = {
            email : this.state.username ,
            password : this.state.password
        }

        this.props.loginRequest(credentials);

    }

    render() {
        const isAuthenticated = this.props.isAuthenticated;
        if(isAuthenticated){
            return (
                <Redirect to="/" />
            )
        }

        return (

        <div className="container">
        <link rel="stylesheet" type="text/css" href="/style/styles.css" />
            <div className="d-flex justify-content-center h-100">
                <div className="card">
                <div className="card-header">
                    <h3>Sign In</h3>
                    <div className="d-flex justify-content-end social_icon">
                    <span><i className="fab fa-facebook-square" /></span>
                    <span><i className="fab fa-google-plus-square" /></span>
                    <span><i className="fab fa-twitter-square" /></span>
                    </div>
                </div>
                <div className="card-body">
                    <form onSubmit={this.onSubmit}>
                        <div className="input-group form-group">
                            <div className="input-group-prepend">
                            <span className="input-group-text"><i className="fas fa-user" /></span>
                            </div>
                            <input id="username" name="username" onChange={this.onChange} value={this.state.username} type="text" className="form-control" placeholder="username" />
                        </div>
                        <div className="input-group form-group">
                            <div className="input-group-prepend">
                            <span className="input-group-text"><i className="fas fa-key" /></span>
                            </div>
                            <input id="password" name="password" onChange={this.onChange} value={this.state.password} type="password" className="form-control" placeholder="password" />
                        </div>
                        <div className="row align-items-center remember">
                            <input type="checkbox" />Remember Me
                        </div>
                        <div className="form-group">
                            <input type="submit" defaultValue="Login" className="btn float-right login_btn" />
                        </div>
                    </form>
                </div>
                <div className="card-footer">
                    <div className="d-flex justify-content-center links">
                    Don't have an account?<a href="/register">Register</a>
                    </div>
                    <div className="d-flex justify-content-center">
                    <a href="/forgot">Forgot your password?</a>
                    </div>
                </div>
                </div>
            </div>
        </div>
            
        )
    }
}
const mapStateToProps = (state) => ({
    isAuthenticated : state.user.isAuthenticated
})

export default connect(mapStateToProps , { loginRequest })(Login);
